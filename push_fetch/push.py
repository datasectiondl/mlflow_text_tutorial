# -*- coding: utf-8 -*-

import os
import sys
import argparse
import data
import payload
import requests
from datetime import datetime as dt

DEMO_PRIORITY = 0


def load(file_path):
    with open(file_path, 'r') as f:
        lines = [l.strip() for l in f.readlines()]
    return lines


def post(text, receiver_end_point, stream_name, token, path):
    text_data = data.new_text_json(text)
    payload_data = payload.new_payload(
        text_data, path, DEMO_PRIORITY, stream_name)
    headers = {'Token': token, 'Content-Type': 'application/json'}
    r = requests.post(receiver_end_point, data=payload_data, headers=headers)
    if r.status_code != 200:
        print('[MLFlow] failed to push. code: %d, file: %s' %
              (r.status_code, path))
        return
    res = r.json()
    print('[MLFlow] success to push. message_id: %s, file: %s' %
          (res['message_id'], path))
    return res['message_id']


def push(receiver_end_point, stream_name, token, files):
    now = dt.now()
    timestamp = now.strftime('%Y%m%d%H%M%S')
    with open('%s.csv' % timestamp, 'w') as f:
        for file_name in files:
            print('[MLFlow] processing file: %s' % file_name)
            lines = load(file_name)
            for l in lines:
                r = post(l, receiver_end_point, stream_name, token, file_name)
                if r == '':
                    continue
                f.write('%s,%s,%s\n' % (file_name, l, r))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MLFlow push python library.')
    parser = argparse.ArgumentParser(description='MLFlow push python library.')
    parser.add_argument('--stream_name', type=str, required=True,
                        help='stream name')
    parser.add_argument('--token', type=str, required=True,
                        help='public key of the MLFlow user')
    parser.add_argument('--text_file', type=str, nargs='+', required=True,
                        help='file path of text data to push MLFlow')
    parser.add_argument('--receiver', type=str, default='https://receiveapi.mlflow.net',
                        help='end point of the receiver')
    args = parser.parse_args()

    print('[MLFlow] push starting...')
    push(args.receiver, args.stream_name, args.token, args.text_file)
    print('[MLFlow] push finish.')
