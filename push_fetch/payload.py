# -*- coding: utf-8 -*-

import json
import base64


def new_payload(data, file_path, priority, stream_name):
    x_dict = {
        "data": data
    }
    x_json = json.dumps(x_dict, ensure_ascii=False)
    print(x_json)

    data_dict = {
        "payload": x_json,
        "file_path": file_path
    }
    data_json = base64.b64encode(json.dumps(
        data_dict, ensure_ascii=False).encode('utf-8'))

    body_dict = {
        "data": data_json.decode("utf-8"),
        "priority": priority,
        "stream_name": stream_name
    }
    body_json = json.dumps(body_dict)
    return body_json


def get_raw_from_payload(data):
    payload_dict = json.loads(data.decode('utf-8'))
    data_dict = json.loads(payload_dict['payload'])
    return data_dict['data']
