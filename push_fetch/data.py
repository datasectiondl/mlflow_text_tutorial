import json


def new_text_json(text):
    text_json = {}
    text_json["text"] = [text]
    text_json["information"] = [{}]
    return text_json
