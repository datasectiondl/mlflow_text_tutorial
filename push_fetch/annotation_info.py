# -*- coding: utf-8 -*-

import json


def new_stream_info(stream_name, fixed_flag, modified_flag, deleted_flag):
    annotation_dict = {
        "stream_name": stream_name,
        "fixed_flag": [fixed_flag],
        "modified_flag": [modified_flag],
        "deleted_flag": [deleted_flag]
    }
    annotation_json = json.dumps(annotation_dict)
    return annotation_json


def new_annotation_info(stream_name, message_ids, fixed_flag, modified_flag, deleted_flag):
    # {
    #     "stream_name": "string",
    #     "url": [
    #         "string"
    #     ],
    #     "message_id": [
    #         "string"
    #     ],
    #     "recognized_flag": [
    #         true
    #     ],
    #     "fixed_flag": [
    #         true
    #     ],
    #     "modified_flag": [
    #         true
    #     ],
    #     "deleted_flag": [
    #         true
    #     ],
    #     "priority": 0,
    #     "from": "string",
    #     "to": "string",
    #     "updated_from": "string",
    #     "updated_to": "string",
    #     "expired_at_from": "string",
    #     "expired_at_to": "string",
    #     "deleted_at_from": "string",
    #     "deleted_at_to": "string",
    #     "received_at_from": "string",
    #     "received_at_to": "string",
    #     "limit": 0,
    #     "offset": 0
    # }
    fixed_flags = [fixed_flag for i in range(len(message_ids))]
    modified_flags = [modified_flag for i in range(len(message_ids))]
    deleted_flags = [deleted_flag for i in range(len(message_ids))]
    annotation_dict = {
        "stream_name": stream_name,
        "message_id": message_ids,
        "fixed_flag": fixed_flags,
        "modified_flag": modified_flags,
        "deleted_flag": deleted_flags
    }
    annotation_json = json.dumps(annotation_dict)
    return annotation_json