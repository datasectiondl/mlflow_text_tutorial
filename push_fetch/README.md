# Push/Fetch #

The mlflow_text_tutorial is sample python script to use mlflow easily.

## Pushing data ##
Suppose your data are text,

```
python push.py --stream_name <stream name> --token <public key of the MLFLow user> --text_file <text file>
```

Each line of <text file> represent text sample.  
A <YYYYMMDDHHMMSS>.csv file will be saved as an evidence file of pushing data.


## Fetching data ##
When it is requierd to get annotations and raw data,
2 ways are provided. Those are based on "the evidence file" or "stream name".

```
python fetch.py --token <public key of the MLFLow user>  --output_dir <path to output directory> --evidence <evidence file...>
```
or

```
python fetch.py --token <public key of the MLFLow user> --output_dir <path to output directory> --stream_name <a stream name>
```

When you specify `--evidence`, you can get pushed data.
If `--stream_name` is assigned, you can get all data in stream.


## Exercise
```bash
$ cd TUTORIAL_ROOT/push_fetch

# Push the text
# "token" is the public key of the MLFlow user
$ python push.py --stream_name "***" --token "****" --text_file text/pos_neg.txt
# Check that a evidence CSV file (YYYYMMDDHHMMSS.csv) exists here.

# (Annotation at the MLFlow Console)

# Fetch the annotated text with the evidence file
$ python fetch.py --token "***" --output_dir ../recognizer/fetched_text --evidence "YYYYMMDDHHMMSS.csv"

# Now, You got the text and their annotation JSON files
# under the "TUTORIAL_ROOT/recognizer/fetched_text" directory!
```

## Using docker-compose command
```bash
$ cd TUTORIAL_ROOT
# Push the text
$ docker-compose run --rm app python ./push_fetch/push.py --stream_name "***" --token "****" --text_file ./push_fetch/text/pos_neg.txt

# (Annotation at the MLFlow Console)

# Fetch the annotated text with the evidence file
$ docker-compose run --rm app python ./push_fetch/fetch.py --token "***" --output_dir ./recognizer/fetched_text --evidence "YYYYMMDDHHMMSS.csv"
```

Next step is traning model and deploying recognizer. See [recognizer](../recognizer).
