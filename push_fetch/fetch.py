# -*- coding: utf-8 -*-

import argparse
import csv
import os
import base64
import json
import requests
import raw_info
import payload
import annotation_info

COLUMN_NUM_MESSAGE_ID = 2
USE_FIXED = False
USE_MODIFIED = True
USE_DELETED = False

RAW_SUFIX = '.text.json'
ANNOTATION_SUFIX = '.meta.json'


def get_message_ids(receiver_end_point, token, stream_name):
    annotation_endpoint = '%s/%s' % (receiver_end_point, 'processed-data')
    info = annotation_info.new_stream_info(
        stream_name, USE_FIXED, USE_MODIFIED, USE_DELETED)
    headers = {'Token': token}
    r = requests.post(annotation_endpoint, data=info, headers=headers)
    messages = json.loads(r.content)
    ids = []
    for message in messages:
        ids.append(message['message_id'])
    return ids


def get_raw(receiver_end_point, token, message_id):
    raw_endpoint = '%s/%s/%s' % (receiver_end_point, 'raw', message_id)
    info = raw_info.new_raw_info(False, True)
    headers = {'Token': token}
    r = requests.post(raw_endpoint, data=info, headers=headers)
    raw = payload.get_raw_from_payload(base64.b64decode(r.content))
    return raw


def get_annotation(receiver_end_point, token, stream_name, message_ids):
    annotation_endpoint = '%s/%s' % (receiver_end_point, 'processed-data')
    info = annotation_info.new_annotation_info(
        stream_name, message_ids, USE_FIXED, USE_MODIFIED, USE_DELETED)
    headers = {'Token': token}
    r = requests.post(annotation_endpoint, data=info, headers=headers)
    return json.loads(r.content.decode('utf-8'))


def fetch_from_stream_name(receiver_end_point, token, stream_name, without_raw, output_dir):
    print('stream name: %s' % stream_name)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    ids = get_message_ids(receiver_end_point, token, stream_name)
    for id in ids:
        print('fetching: %s' % id)
        try:
            fetch_from_id(receiver_end_point, token,
                          id, without_raw, output_dir)
        except Exception as e:
            print('warning: %s', e)
    annotations = get_annotation(receiver_end_point, token, stream_name, ids)
    for annotation in annotations:
        with open(os.path.join(output_dir, '%s%s' % (annotation['message_id'], ANNOTATION_SUFIX)), 'w') as a:
            a.write(annotation['train_metadata'])


def fetch_from_id(receiver_end_point, token, id, without_raw, output_dir):
    if not without_raw:
        # Get raw data
        raw = get_raw(receiver_end_point, token, id)
        with open(os.path.join(output_dir, '%s%s' % (id, RAW_SUFIX)), 'w') as r:
            json.dump(
                raw, r, indent=4,
                separators=(',', ': '), ensure_ascii=False
            )


def fetch_from_ids(receiver_end_point, token, paths, without_raw, output_dir):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for path in paths:
        message_ids = []
        stream_name = ''
        with open(path, 'r') as f:
            reader = csv.reader(f)
            for i, row in enumerate(reader):
                if i == 0:
                    stream_name = row[2].split('.')[0]

                print('fetching: %s' % row[COLUMN_NUM_MESSAGE_ID])
                try:
                    fetch_from_id(
                        receiver_end_point, token, row[COLUMN_NUM_MESSAGE_ID], without_raw, output_dir)
                    message_ids.append(row[COLUMN_NUM_MESSAGE_ID])
                except Exception as e:
                    print('warning: %s' % e)

        annotations = get_annotation(
            receiver_end_point, token, stream_name, message_ids)
        for annotation in annotations:
            with open(os.path.join(output_dir, '%s%s' % (annotation['message_id'], ANNOTATION_SUFIX)), 'w') as a:
                a.write(annotation['train_metadata'])


def fetch(receiver_end_point, token, stream_name, paths, without_raw, output_dir):
    if stream_name == '' or stream_name is None:
        fetch_from_ids(receiver_end_point, token,
                       paths, without_raw, output_dir)
    else:
        fetch_from_stream_name(receiver_end_point, token,
                               stream_name, without_raw, output_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MLFlow fetch python library.')
    parser.add_argument('--token', type=str, required=True,
                        help='public key of the MLFlow user')
    parser.add_argument('--output_dir', required=True,
                        type=str, help='output directory for downloaded raw data.')
    parser.add_argument('--evidence', metavar='', type=str, nargs='+',
                        help='file path of message_id to fetch data from MLFlow. '
                             'This option process ignore stream name.')
    parser.add_argument('--stream_name', type=str,
                        help='stream name for downloading raw data')
    parser.add_argument('--datamanager', type=str, default='https://datamanager.mlflow.net',
                        help='end point of the datamanager')
    parser.add_argument('--without_raw', action='store_true',
                        help='True: get raw data and annotations. False: get only annotations.')

    args = parser.parse_args()

    print('[MLFlow] fetch starting...')
    fetch(args.datamanager, args.token, args.stream_name,
          args.evidence, args.without_raw, args.output_dir)
    print('[MLFlow] fetch finish.')
