# -*- coding: utf-8 -*-

import json


def new_raw_info(encrypted, self_sender):
    raw_dict = {
        "encrypted": encrypted,
        "self_sender": self_sender
    }
    raw_json = json.dumps(raw_dict)
    return raw_json
