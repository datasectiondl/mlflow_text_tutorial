# Model training

Let's create a text classification model.
We are going to use Simple RNN model here.

## Exercise
```bash
$ cd TUTORIAL_ROOT/recognizer

# Create the necessary 3 files for training the model
#   1. JSON file which includes category-index information
#   2. Training CSV file
#   3. Validation CSV file
$ python create_csv_and_category_index.py \
  --extra_json extra_info.json \
  --fetched_dir fetched_text \
  --category_json category_index.json

# Now, You can find "category_index.json", "training.csv", "validation.csv"

# Let's start training of RNN classification model!
$ python train.py \
  --category_json category_index.json \
  --training_csv training.csv \
  --validation_csv validation.csv \
  --model_output_dir model
```
While training, learned parameters are
  periodically saved in the ```model_output_dir```.

### Using docker-compose command

```bash
$ cd TUTORIAL_ROOT/recognizer

# Create the necessary 3 files for training the model
#   1. JSON file which includes category-index information
#   2. Training CSV file
#   3. Validation CSV file
$ docker-compose run --rm trainer python create_csv_and_category_index.py \
  --category_json category_index.json \
  --extra_json extra_info.json \
  --fetched_dir fetched_text \
  --train_csv training.csv \
  --val_csv validation.csv

# Now, You can find "category_index.json", "training.csv", "validation.csv"

# Let's start training of RNN classification model!
$ docker-compose run --rm trainer python train.py \
  --category_json category_index.json \
  --training_csv training.csv \
  --validation_csv validation.csv \
  --model_output_dir model
```

# Recognizer
Let's set up recognition server (we call this "recognizer") using the learned model.

## Exercise
```bash
$ cd TUTORIAL_ROOT/recognizer

# If you want change port of server to access, 
# modify PORT_HOST in '.env' file.

# Let's build docker image for the recognizer
$ docker-compose build
# Run the recognizer
$ docker-compose run --service-ports --name text_recognizer recognizer 
# Now, recognizer is up at localhost:8080/text_recognizer


# Let's throw an text to the recognizer and check the results!
# Open the another terminal
$ cd TUTORIAL_ROOT/recognizer
$ python post_test_text.py \
  --recognizer http://localhost:8080/recognizer \
  --text test_data/pos_neg_test.txt
```

### Using docker-compose command
```bash
$ cd TUTORIAL_ROOT/recognizer

# If you want change port of server to access, 
# modify PORT_HOST in '.env' file.

# Let's build docker image for the recognizer
$ docker-compose build
# Run the recognizer
$ docker-compose run --service-ports --name text_recognizer recognizer 
# Now, recognizer is up at text_recognizer:8989/recognizer


# Let's throw an text to the recognizer and check the results using docker.
# Open the another terminal
$ cd TUTORIAL_ROOT/recognizer
# Attention: end point of the recognizer is different from not-docker version.
$ docker-compose run --rm recognizer python post_test_text.py  --recognizer http://text_recognizer:8989/recognizer \
--text test_data/pos_neg_test.txt
```
"text_file_path" is path to text file. Each line of its file represents sample input.
