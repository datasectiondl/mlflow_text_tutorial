import os
import json

import dill
import tornado.ioloop
import tornado.web
import ujson
import argparse
from helper.request import Request
from helper.status import Status
from helper.annotateTextResponse import AnnotateTextResponse
from helper.response import Response
from helper.textHelper import url2text, string2text
from helper.logs import MyLog

from text_classification import model_utils

MODULE_NAME = 'recognizer'
MyLog.initialize('.', MODULE_NAME)


class StatusHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_status(200)
        self.finish()
        return


class ServerHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', 'x-requested-with')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def get(self):
        MyLog.write(MODULE_NAME, level='INFO', message='Got GET.')
        self.set_status(200)
        self.finish()
        return

    def post(self):
        MyLog.write(MODULE_NAME, level='INFO', message='Got POST.')

        request_body = tornado.escape.json_decode(self.request.body)
        request_data = Request.serialize(request_body)

        response = Response([])
        for annotate_text_req in request_data.requests:
            annotate_data = annotate_text_req.data
            annotate_features = annotate_text_req.features
            url, content = (None,)*2
            if hasattr(annotate_data, 'content'):
                content = annotate_data.content
            if hasattr(annotate_data, 'url'):
                url = annotate_data.url

            try:
                # Get text from url
                if content is not None:
                    content_dict = json.loads(content.encode('utf-8'))
                    text = content_dict['text']
                if url is not None:
                    text, _ = url2text(url)

            except Exception as err:
                print(err, flush=True)
                status = Status(code=400, message=err)
                annotate_text_res = AnnotateTextResponse()
                response.add(annotate_text_res)
                MyLog.write(MODULE_NAME, level='ERROR', message=err)
                self.finish(ujson.dumps(response))
                return

            # Prediction
            for feature in annotate_features:
                if feature.type == 'TEXT_CLASSIFICATION':
                    try:
                        classes = predictor.inference(text)
                        if url is not None:
                            message_id = os.path.basename(url)
                            annotate_text_res = AnnotateTextResponse(
                                message_id,
                                classes,
                            )
                        else:
                            annotate_text_res = [
                                {"text": t, "class": c[0]} for t, c in zip(text, classes)]

                        response.add(annotate_text_res)
                        MyLog.write(MODULE_NAME, level='INFO',
                                    message='Successfully predicted. (prediction: %s)' % classes)
                    except Exception as err:
                        print(err, flush=True)
                        status = Status(
                            code=500, message='Internal Server Error. Please contact admin!')
                        annotate_text_res = AnnotateTextResponse()
                        response.add(annotate_text_res)
                        MyLog.write(
                            MODULE_NAME, 'Recognizer has problems! %s!' % format(err))
                        break

        self.finish(ujson.dumps(response, ensure_ascii=False))


def make_app():
    settings = {
        'debug': True,
        'autoreload': True
    }
    return tornado.web.Application([
        (r'/status', StatusHandler),
        (r'/recognizer', ServerHandler),
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': 'static'})
    ], **settings)


def get_args():
    parser = argparse.ArgumentParser(description='Recognizer args...')
    parser.add_argument('--meta_file', type=str, required=True,
                        help='absolute path to the model meta file')
    parser.add_argument('--category_json', type=str, required=True,
                        help='absolute path to the category json file')
    parser.add_argument('--tokenizer', type=str, required=True,
                        help='absolute path to the tokenizer')
    parser.add_argument('--port', type=int, required=True, help='port')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = get_args()

    with open(args.tokenizer, 'rb') as f:
        tokenizer = dill.load(f)
    with open(args.category_json, 'rb') as f:
        category_dict = json.load(f)
    # Load model
    predictor = model_utils.PredictionModel(
        args.meta_file,
        category_dict,
        tokenizer,
    )

    app = make_app()
    app.listen(args.port)

    print('Server is up at %d...' % args.port)
    print('If you run with docker, access to the \"HOST_PORT\" port which is written in the \".env\" file')
    tornado.ioloop.IOLoop.current().start()
