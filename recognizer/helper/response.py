#!/usr/bin/env python
# -*- coding: utf-8 -*-

from helper.annotateTextResponse import AnnotateTextResponse


class Response(object):
    def __init__(self, responses=[]):
        self.__responses = responses

    @property
    def responses(self):
        return self.__responses

    @responses.setter
    def responses(self, responses):
        self.__responses = responses

    def add(self, annotateTextRes):
        self.__responses.append(annotateTextRes)
