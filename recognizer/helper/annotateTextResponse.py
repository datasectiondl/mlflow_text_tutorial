class AnnotateTextResponse(object):
    def __init__(self, message_id=None, classes=None):
        if message_id is not None:
            self.__message_id = message_id
        if classes is not None:
            self.__classes = classes

    @property
    def message_id(self):
        return self.__message_id

    @message_id.setter
    def message_id(self, value):
        self.__message_id = value

    @message_id.deleter
    def message_id(self):
        del self.__message_id

    @property
    def classes(self):
        return self.__classes

    @classes.setter
    def classes(self, value):
        self.__classes = value

    @classes.deleter
    def classes(self):
        del self._classes
