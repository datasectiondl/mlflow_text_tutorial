from helper.data import Data
from helper.feature import Feature


class AnnotateTextRequest(object):
    def __init__(self, data, features):
        self.__data = data
        self.__features = features

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, data):
        self.__data = data

    @property
    def features(self):
        return self.__features

    @features.setter
    def features(self, features):
        self.__features = features

    @staticmethod
    def serialize(dic):
        data, features = None, []
        if 'data' in dic:
            data = Data.serialize(dic['data'])
        if 'features' in dic:
            for f in dic['features']:
                feature = Feature.serialize(f)
                features.append(feature)
        return AnnotateTextRequest(data, features)
