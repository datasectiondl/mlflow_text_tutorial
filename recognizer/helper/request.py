#!/usr/bin/env python
# -*- coding: utf-8 -*-

from helper.annotateTextRequest import AnnotateTextRequest


class Request(object):
    def __init__(self, requests, date, key):
        self.__requests = requests
        self.__date = date
        self.__key = key

    @property
    def requests(self):
        return self.__requests

    @requests.setter
    def requests(self, requests):
        self.__requests = requests

    @property
    def date(self):
        return self.__date

    @date.setter
    def date(self, date):
        self.__date = date

    @property
    def key(self):
        return self.__key

    @key.setter
    def key(self, key):
        self.__key = key

    @staticmethod
    def serialize(dic):
        requests, date, key = [], None, None
        if 'requests' in dic:
            for req in dic['requests']:
                request = AnnotateTextRequest.serialize(req)
                requests.append(request)
        if 'date' in dic:
            date = dic['date']
        if 'key' in dic:
            key = dic['key']
        return Request(requests, date, key)
