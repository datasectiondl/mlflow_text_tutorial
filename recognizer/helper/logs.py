#!/usr/bin/env python
#encoding: utf-8

import os, os.path
import logging
from datetime import datetime
from logging import FileHandler
from logging import getLogger, INFO, ERROR
import ujson
import os
# TODO get settings from logs directory
# TODO change file name by execute

#this class is written in light of workflow_t0001/logs.py


class LogMessage(object):
    def __init__(self):
        self.__create_time = {}
        self.__time = None
        self.__module = None
        self.__level = None
        #self.__file_name = None
        self.__message = ''
        self.__log_dir = ''
        pass

    def initialize(self, output_dir, module, level='INFO'):
        if output_dir is None or module is None:
            # TODO throw exception
            pass

        tdatetime = datetime.now()
        self.__create_time[module] = tdatetime.strftime('%Y-%m-%d %H-%M-%S.%f')[:-3]
        self.module = module
        self.level = level

        self.log_dir = os.path.join(output_dir, 'logs')
        # log dir
        if not os.path.exists(self.log_dir):
            os.mkdir(self.log_dir)

    @property
    def log_dir(self):
        return self.__log_dir

    @log_dir.setter
    def log_dir(self, value):
        self.__log_dir = value

    @property
    def create_time(self, module):
        return self.__create_time[module]

    @create_time.setter
    def create_time(self, value, module):
        self.__create_time[module] = value

    @property
    def time(self):
        return self.__time

    @time.setter
    def time(self, value):
        self.__time = value

    @property
    def module(self):
        return self.__module

    @module.setter
    def module(self, value):
        self.__module = value

    @property
    def level(self):
        return self.__level

    @level.setter
    def level(self, value):
        self.__level = value




    @property
    def message(self):
        return self.__message

    @message.setter
    def message(self, value):
        self.__message = value

    def output(self):
        dict = {}
        dict["time"] = self.time
        dict["module"] = self.module
        dict["level"] = self.level
        dict["message"] = self.message
        return (dict["time"]+'\t'+ujson.dumps(dict))

    def write(self, module, level='INFO', message=''):
        if module is None:
            # TODO throw exception
            pass

        tdatetime = datetime.now()
        self.time = tdatetime.strftime('%Y-%m-%d %H-%M-%S.%f')[:-3]
        self.module = module
        self.level = level.upper()
        self.message = message

        if ('INFO' == level.upper()):
            # common standard log file handler
            file_logger = getLogger(module + '_std')
            file_logger.setLevel(INFO)
            if len(file_logger.handlers) == 0:
                log_file = os.path.join(self.log_dir, module + '_' + self.__create_time[module] + '.log')
                file_handler = FileHandler(filename=log_file, mode='a')
                file_logger.addHandler(file_handler)
            file_logger.info(self.output())
        elif ('ERROR' == level.upper()):
            # common error log file handler
            error_logger = getLogger(module + '_error')
            error_logger.setLevel(ERROR)
            if len(error_logger.handlers) == 0:
                error_file = os.path.join(self.log_dir, module + '_' + self.__create_time[module] + '.error')
                error_handler = FileHandler(filename=error_file, mode='a')
                error_logger.addHandler(error_handler)
            error_logger.error(self.output())

MyLog = LogMessage()

if __name__ == "__main__":
    test_dir=os.getcwd()
    MyLog.initialize("","TEST")
    MyLog.write("TEST","ERROR","aiueo")
    MyLog.write("TEST","info","aiueo")