# -*- coding: utf-8 -*-

import base64
import urllib.request
import json

from helper.logs import MyLog

MODULE_NAME = 'text_helper'
MyLog.initialize('.', MODULE_NAME)


def string2text(json_bytes):
    try:
        raw_decode = base64.b64decode(json_bytes.decode('utf-8'))
        body = json.loads(raw_decode, encoding='utf-8')
        body_pay_dict = json.loads(body['payload'], encoding='utf-8')
        data = body_pay_dict['data']
        text = data['text']
        information = data['information']

        return text, information

    except Exception as err:
        MyLog.write(MODULE_NAME, level='ERROR', message=err)
        raise err


def url2text(url):
    try:
        raw = urllib.request.urlopen(url).read()
        return string2text(raw)

    except Exception as err:
        MyLog.write(MODULE_NAME, level='ERROR', message=err)
        raise err
