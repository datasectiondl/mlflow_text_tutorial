# -*- coding: utf-8 -*-

import argparse
import base64
import json

import requests


def main():
    text_path = args.text
    recog_end_point = args.recognizer
    with open(text_path, 'r') as f:
        text = [l.strip() for l in f.readlines()]
    text_jsons = [{"text": [t], "information": [{}]} for t in text]

    for t_j in text_jsons:
        enc_json = json.dumps(t_j, ensure_ascii=False)
        data = {'requests': [{'data': {'content': enc_json},
                              'features': [{'type': 'TEXT_CLASSIFICATION'}]}]}
        data = json.dumps(data, ensure_ascii=False).encode('utf-8')
        r = requests.post(recog_end_point, data=data)

        if r.status_code != 200:
            print('Test failed. code: %d, file: %s' %
                  (r.status_code, text_path))
            return

        res = r.json()['responses']
        print("text: {}, class: {}".format(
            res[0][0]['text'], res[0][0]['class']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--recognizer', type=str, required=True,
                        help='end point of the recognizer (e.g. http://localhost:8080/recognizer)')
    parser.add_argument('--text', type=str, required=True,
                        help='path to the test text')
    args = parser.parse_args()

    main()
