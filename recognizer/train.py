"""Runs a ResNet model on the fetched data from MLFlow."""

import argparse
import json
import os
import dill
import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.utils.data import DataLoader

from text_classification import model, train_utils, text_utils

TOKENIZER_FILE = 'tokenizer.pk'
# MOMENTUM = 0.9
# WEIGHT_DECAY = 1e-4

parser = argparse.ArgumentParser()

parser.add_argument(
    '--category_json', type=str, required=True,
    help='The json file which includes category information')

parser.add_argument(
    '--training_csv', type=str, required=True,
    help='The csv file which includes path to training data and its labels')

parser.add_argument(
    '--validation_csv', type=str, required=True,
    help='The csv file which includes path to validation data and its labels')

parser.add_argument(
    '--model_output_dir', type=str, required=True,
    help='The directory where the model will be stored.')

parser.add_argument(
    '--n_layer', type=int, default=1, help='The number of RNN layer.')

parser.add_argument(
    '--n_cell', type=int, default=32, help='The number of RNN cell.')

parser.add_argument(
    '--cell_type', type=str, default='LSTM', choices=['LSTM', 'GRU'],
    help='The type of RNN.')

parser.add_argument('--dropout', action='store_true',
                    help='True: dropout will be used. (0.5 for intermediate layer, 0.2 for embedding)')

parser.add_argument(
    '--embedding_dim', type=int, default=200, help='The number of embedding dimention.')

parser.add_argument(
    '--train_epochs', type=int, default=100,
    help='The number of epochs to use for training.')

parser.add_argument(
    '--epochs_per_eval', type=int, default=1,
    help='The number of training epochs to run between evaluations.')
parser.add_argument(
    '--epochs_per_save', type=int, default=10,
    help='The number of training epochs to run between evaluations.')

parser.add_argument(
    '--batch_size', type=int, default=1,
    help='Batch size for training and evaluation.')

parser.add_argument(
    '--max_sentence', type=int, default=20,
    help='The max length of sentence.')

parser.add_argument(
    '--token_unit', type=str, default='word', choices=['word', 'char'],
    help='The unit of token.')


def load_tokenizer(tokenizer_file, unit, max_length):
    if os.path.isfile(tokenizer_file):
        with open(tokenizer_file, 'rb') as f:
            tokenizer = dill.load(f)
    else:
        tokenizer = text_utils.WordCharTokenizer(unit, max_length)

    return tokenizer


if __name__ == '__main__':
    args = parser.parse_args()
    tokenizer = load_tokenizer(
        TOKENIZER_FILE, args.token_unit, args.max_sentence)
    with open(args.category_json, 'r') as f:
        category_dict = json.load(f)
    train_dataset = train_utils.PNDataset(
        args.training_csv, category_dict,
        tokenizer,
    )
    val_dataset = train_utils.PNDataset(
        args.validation_csv, category_dict,
        tokenizer, registration=False,
    )
    train_loader = DataLoader(train_dataset, args.batch_size, shuffle=True)

    with open(TOKENIZER_FILE, 'wb') as f:
        dill.dump(tokenizer, f)
    n_dict = len(tokenizer.token_dict.keys())+1
    rnn_classifier = model.RNNTextClassifier(
        n_dict=n_dict,
        embedding_dim=args.embedding_dim,
        n_layer=args.n_layer,
        n_rnn_cell=args.n_cell,
        n_class=2,
        cell_type=args.cell_type,
        dropout=args.dropout,
    )
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(rnn_classifier.parameters())
    t_loss, v_loss = train_utils.iterate_train(
        model=rnn_classifier,
        train_loader=train_loader,
        val_dataset=val_dataset,
        criterion=criterion,
        optimizer=optimizer,
        n_epoch=args.train_epochs,
        epochs_per_eval=args.epochs_per_eval,
        epochs_per_save=args.epochs_per_save,
        save_dir=args.model_output_dir
    )
