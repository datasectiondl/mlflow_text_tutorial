import unicodedata
import re

from janome.tokenizer import Tokenizer

UNKNOWN_INDEX = 1


def normalize(text):
    norm_text = unicodedata.normalize('NFKC', text)
    norm_text = re.sub(r'\d+', '0', norm_text)
    norm_text = norm_text.lower()
    return norm_text


class WordCharTokenizer():

    def __init__(self, token_unit='word', max_length=20,
                 norm=True, base_form=False):
        if token_unit not in {'word', 'char'}:
            raise RuntimeError("{} is not supported".format(token_unit))

        self.unit = token_unit
        self.tokenizer = Tokenizer()
        self.max_length = max_length
        self.norm = norm
        self.base_form = base_form
        self.token_dict = {'UNK': {'count': -1, 'index': UNKNOWN_INDEX}}
        self.last_index = 2

    def __call__(
        self, text: str, pad=False, pad_index=0, registration=True
    ):
        if pad and self.max_length is None:
            raise RuntimeError("You must specify max_length")
        if not text:
            return []

        try:
            if self.norm:
                text = normalize(text)

            if self.unit == 'word':
                indices = self._make_word_indices(text, registration)
            else:
                indices = self._make_char_indices(text, registration)

            length = len(indices)
            if length > self.max_length:
                indices = indices[:self.max_length]
                length = self.max_length
            if length < self.max_length:
                indices = indices + [pad_index]*(self.max_length-length)

        except Exception as e:
            print("Error ocured in tokenizer: {}".format(e))
            return []

        return indices, length

    def _make_word_indices(self, text: str, registration=True):
        w_index = []
        for token in self.tokenizer.tokenize(text):
            w = token.base_form if self.base_form else token.surface
            if registration:
                w_index.append(self._register_dict(w))
            else:
                index_count = self.token_dict.get(w, self.token_dict['UNK'])
                w_index.append(index_count['index'])

        return w_index

    def _make_char_indices(self, text: str, registration=True):
        ch_index = []
        for c in text:
            if registration:
                ch_index.append(self._register_dict(c))
            else:
                index_count = self.token_dict.get(c, self.token_dict['UNK'])
                ch_index.append(index_count['index'])

        return ch_index

    def _register_dict(self, token: str):
        if token in self.token_dict:
            index_count = self.token_dict[token]
            index = index_count['index']
            index_count['count'] += 1
        else:
            index = self.last_index
            index_count = {'index': index, 'count': 1}
            self.last_index += 1
            self.token_dict[token] = index_count

        return index
