import pickle as pk
import os

import pandas as pd
import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.utils.data import Dataset

from .text_utils import WordCharTokenizer

INDEXED_FILE_PREFIX = 'indexed'


class PNDataset(Dataset):
    """Positive Negative Dataset"""

    def __init__(self, csv_file, category_dict, tokenizer, registration=True):
        """
        Arguments:
            csv_file {str} -- Path to the csv file with text.
            category_dict {dict} -- Dictionary of {'category', 'index'}.
            tokenizer {WordCharTokenizer} -- Instance of WordCharTokenizer.
            registration {bool} -- Flag for tokenizer.
                If True, new word in csv file is registered in token dict.
        """

        dir_name, file_name = os.path.split(csv_file)
        file_prefix = file_name.split('.')[0]
        indexed_file_name = INDEXED_FILE_PREFIX + "-" + file_prefix + '.json'
        indexed_file = os.path.join(dir_name, indexed_file_name)
        if os.path.isfile(indexed_file):
            self.data = pd.read_json(
                indexed_file,
                orient='records',
                lines=True,
            )
        else:
            raw_data = pd.read_csv(csv_file, header=None, delimiter=',')
            indexed_text = []
            lengths = []
            labels = []
            for _, row in raw_data.iterrows():
                label = category_dict[row[0]]
                tokens = tokenizer(
                    row[1], pad=True,
                    registration=registration
                )
                indices, l = tokens
                indexed_text.append(indices)
                lengths.append(l)
                labels.append(label)
            self.data = pd.DataFrame({
                'label': labels,
                'length': lengths,
                'indexed_text': indexed_text,
            }, columns=['label', 'length', 'indexed_text']
            )
            self.data.to_json(
                indexed_file,
                orient='records',
                lines=True,
            )

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        tmp_data = self.data.ix[idx, :]
        x = torch.LongTensor(tmp_data["indexed_text"])
        y = tmp_data['label'].tolist()
        length = tmp_data["length"].tolist()
        return {'x': x, 'y': y, 'length': length}


def sort_input(x, y, length):
    length, sort_index = length.sort(dim=0, descending=True)
    x = x.index_select(dim=0, index=sort_index).t()
    x = Variable(x, requires_grad=False)
    y = y.index_select(dim=0, index=sort_index)
    y = Variable(y, requires_grad=False)
    return x, y, length.tolist()


def train(model, loader, criterion, optimizer):
    epoch_loss = 0
    counter = 0
    for v in loader:
        optimizer.zero_grad()
        x, y, length = sort_input(v['x'], v['y'], v['length'])
        output = model(x, length)
        loss = criterion(output, y)
        loss.backward()
        optimizer.step()
        epoch_loss += loss.data
        counter += 1
    return epoch_loss / counter


def iterate_train(
        model, train_loader, val_dataset,
        criterion, optimizer, n_epoch,
        epochs_per_eval, epochs_per_save,
        save_dir,
):
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)

    save_path = os.path.join(save_dir, type(model).__name__)
    train_losses = []
    val_losses = []
    val_data = val_dataset[:]
    for i in range(1, n_epoch+1):
        t_loss = train(model, train_loader, criterion, optimizer)
        print('train loss: {}'.format(t_loss[0]))
        train_losses.append(t_loss[0])
        if i % epochs_per_eval == 0:
            x = torch.LongTensor(val_data['x'])
            y = torch.LongTensor(val_data['y'])
            length = torch.LongTensor(val_data['length'])
            x, y, length = sort_input(x, y, length)
            model.eval()
            val_l = criterion(model(x, length), y).data[0]
            val_losses.append(val_l)
            print('validation loss: {}'.format(val_l))
            model.train()
        if i % epochs_per_save == 0:
            torch.save(model, save_path+'_'+str(i)+'.model')
            torch.save(model, save_path+'.model')

    return train_losses, val_losses
