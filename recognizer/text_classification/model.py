import torch
from torch import nn
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.nn.functional as F


class RNNTextClassifier(nn.Module):

    def __init__(self, n_dict: int, embedding_dim: int, n_layer: int, n_rnn_cell: int, n_class: int, *, cell_type='LSTM', dropout=False) -> None:

        if cell_type == 'LSTM':
            rnn_layer = nn.LSTM
        elif cell_type == 'GRU':
            rnn_layer = nn.GRU
        else:
            raise RuntimeError("{} isn't supported".format(cell_type))

        super().__init__()
        self.cell_type = cell_type
        self.embeddings = nn.Embedding(n_dict, embedding_dim, padding_idx=0)
        self.emb_dropout = nn.Dropout(0.2) if dropout else None
        if dropout:
            self.rnn_layer = rnn_layer(
                embedding_dim, n_rnn_cell, n_layer, dropout=0.5)
        else:
            self.rnn_layer = rnn_layer(embedding_dim, n_rnn_cell, n_layer)

        self.linear = nn.Linear(n_rnn_cell, n_class)

    def forward(self, input_x: Variable, length: list):
        """

        [description]

        Arguments:
            input_x: Variable {[int]} -- A (T, B) shaped Variable. Each value of index of word (padding index is -1).
                                    The sequences should be sorted by length with decreasing order.
            length: list {[int]} -- A list of int represent each sentence length.
        """
        x = self.embeddings(input_x)
        x = self.eb_dropout(x) if self.emb_dropout is not None else x
        init_hidden = self.get_init_hidden(input_x.size()[1])
        pack_x = pack_padded_sequence(x, length)
        _, hidden = self.rnn_layer(pack_x, init_hidden)
        x = hidden[-1] if self.cell_type == "GRU" else hidden[0][-1]
        output = self.linear(x)
        return output

    def get_init_hidden(self, n_batch: int):
        num_layers = self.rnn_layer.num_layers
        hidden_size = self.rnn_layer.hidden_size
        h = Variable(
            torch.zeros(num_layers, n_batch, hidden_size),
            requires_grad=True
        )
        if self.cell_type == 'LSTM':
            c = Variable(
                torch.zeros(num_layers, n_batch, hidden_size),
                requires_grad=True
            )
            return (h, c)
        else:
            return h
