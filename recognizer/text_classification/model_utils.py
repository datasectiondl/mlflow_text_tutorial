import torch
from torch.autograd import Variable


class PredictionModel():
    def __init__(self, model_path, category_dict, tokenizer):
        self.inverse_dict = {}
        for k, v in category_dict.items():
            self.inverse_dict[v] = k

        self.model = torch.load(model_path)
        n_classes = set(range(self.model.linear.out_features))
        keys = set(self.inverse_dict.keys())
        if n_classes in keys:
            raise RuntimeError(
                "category don't have {} class key".format(n_classes)
            )
        self.model.eval()
        self.tokenizer = tokenizer

    def inference(self, text):
        classes = []
        for t in text:
            index, length = self.tokenizer(t, registration=False)
            x = Variable(torch.LongTensor(index).view(-1, 1), volatile=True)
            output = self.model(x, [length])[0]
            _, max_i = output.max(dim=0)
            classes.append([self.inverse_dict[max_i.data[0]]])
        return classes
