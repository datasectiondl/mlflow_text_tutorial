# -*- coding: utf-8 -*-

import argparse
import csv
import glob
import json
import os
import random


def main():
    fetched_dir = args.fetched_dir
    train_csv = args.train_csv
    val_csv = args.val_csv

    if not os.path.exists(fetched_dir):
        raise RuntimeError("fetched_dir: {} is not found".format(fetched_dir))

    print('Fetched directory:', fetched_dir)

    data_files = []
    ann_files = []
    paths = glob.glob(os.path.join(fetched_dir, '*'))
    for p in paths:
        file_prefix, suffix = os.path.splitext(p)
        if file_prefix.endswith('.text'):
            data_files.append(p)
        elif file_prefix.endswith('.meta'):
            ann_files.append(p)

    with open(args.extra_json, 'r') as f:
        tmp = json.load(f)
        categories = tmp['objects']

    # Create json file which has category-index information
    cate_dict = {}
    for i, category in enumerate(categories):
        cate_dict[category] = i

    with open(args.category_json, 'w') as f:
        json.dump(cate_dict, f)

    # Create training/validation csv files
    name_label = []
    ann_files.sort()
    data_files.sort()
    for ann_p, data_p in zip(ann_files, data_files):
        with open(ann_p, 'r') as f:
            ann_dic = json.load(f)
        with open(data_p, 'r') as f:
            data_dic = json.load(f)

        for ann, text in zip(ann_dic['classes'], data_dic['text']):
            name_label.append([ann[0], text])

    random.shuffle(name_label)
    # 60 % is for training, 40 % is for validation
    train_num = int(len(name_label) * 0.6)
    with open(train_csv, 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows(name_label[:train_num])
    with open(val_csv, 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows(name_label[train_num:])

    print('Category index:', args.category_json)
    print('Training csv:', train_csv)
    print('Validation csv:', val_csv)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fetched_dir', required=True,
                        type=str, help='path to the fetched directory')
    parser.add_argument('-e', '--extra_json', required=True,
                        type=str, help='path to the json which includes the class definition')
    parser.add_argument('-o', '--category_json', default='category_index.json',
                        type=str, help='path to the output json file')
    parser.add_argument('-t', '--train_csv', default='training.csv',
                        type=str, help='path to the training csv file')
    parser.add_argument('-v', '--val_csv', default='validation.csv',
                        type=str, help='path to the validation csv file')
    parser.add_argument('-p', '--tokenizer_path', default='tokenizer.pk',
                        type=str, help='path to the tokenizer instance')
    args = parser.parse_args()

    main()
